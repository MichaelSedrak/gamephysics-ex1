#include "MassSpringSystemSimulator.h"
#include <map>

MassSpringSystemSimulator::MassSpringSystemSimulator()
{
	this->m_fMass = 10.0;
	this->m_fDamping = 0.0;
	this->m_fStiffness = 40.0;
	this->m_iIntegrator = EULER;
	this->m_fSphereSize = 0.05f;
	this->m_iboundary = 500;
	this->gravity = Vec3(0, 0, 0);
	this->fixpoints = true;
}

const char* MassSpringSystemSimulator::getTestCasesStr()
{
	return "Basic Testcase,Scenario 1,Scenario 2";
}

const char* MassSpringSystemSimulator::getIntegratorsStr()
{
	return "Euler,Leap Frog,Mid Point";
}

void MassSpringSystemSimulator::initUI(DrawingUtilitiesClass* DUC)
{
	this->DUC = DUC;
	TwType TW_TYPE_TESTCASE = TwDefineEnumFromString("Integrator", this->getIntegratorsStr());
	TwAddVarRW(this->DUC->g_pTweakBar, "Integrator", TW_TYPE_TESTCASE, &m_iIntegrator, "");
	TwAddVarRW(this->DUC->g_pTweakBar, "FixPoints", TW_TYPE_BOOLCPP, &fixpoints, "");
	TwAddVarRW(this->DUC->g_pTweakBar, "Boundary", TW_TYPE_INT32, &m_iboundary, "");
}

void MassSpringSystemSimulator::reset()
{
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
}

void MassSpringSystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{
	DUC->setUpLighting(Vec3(), 0.4 * Vec3(1, 1, 1), 100, 0.6 * Vec3(0.97, 0.86, 1));
	for (MassPoint& mpoint : m_masspoints)
		DUC->drawSphere(mpoint.m_position , Vec3(m_fSphereSize, m_fSphereSize, m_fSphereSize));
	
	for (Spring& spring : m_springs)
	{
		DUC->beginLine();
		DUC->drawLine(m_masspoints[spring.m_point1index].m_position, Vec3(0, 0, 1), m_masspoints[spring.m_point2index].m_position, Vec3(1, 1, 1));
		DUC->endLine();
	}
}

void MassSpringSystemSimulator::notifyCaseChanged(int testCase)
{
	switch (testCase)
	{
	case 0:
		cout << "Simple Testcase\n";
		simpleCase();
		break;
	case 1:
		cout << "Scenario 1\n";
		createComplexCase(5,10);
		break;
	case 2:
		cout << "Scenario 2\n";
		createComplexCase(10, 15);
		break;
	default:
		std::cout << "Test Case Error!\nCan only support 0:Euler\t1:Midpoint\t2:LeapFrog.\n";
		break;
	}
}

void MassSpringSystemSimulator::externalForcesCalculations(float timeElapsed)
{
	// Apply the mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
	Point2D mouseDiff;
	mouseDiff.x = m_trackmouse.x - m_oldtrackmouse.x;
	mouseDiff.y = m_trackmouse.y - m_oldtrackmouse.y;
	if (mouseDiff.x != 0 || mouseDiff.y != 0)
	{
		Mat4 worldViewInv = Mat4(DUC->g_camera.GetWorldMatrix() * DUC->g_camera.GetViewMatrix());
		worldViewInv = worldViewInv.inverse();
		Vec3 inputView = Vec3((float)mouseDiff.x, (float)-mouseDiff.y, 0);
		Vec3 inputWorld = worldViewInv.transformVectorNormal(inputView);
		// find a proper scale!
		float inputScale = 0.001f;
		inputWorld = inputWorld * inputScale;
		m_vfMovableObjectPos = m_vfMovableObjectFinalPos + inputWorld;
	}
	else {
		m_vfMovableObjectFinalPos = m_vfMovableObjectPos;
	}
}

void MassSpringSystemSimulator::simulateTimestep(float timeStep)
{
	switch (m_iIntegrator)
	{
	case EULER:
		for (auto& spring : m_springs)
		{
			MassPoint& p1 = m_masspoints[spring.m_point1index];
			MassPoint& p2 = m_masspoints[spring.m_point2index];

			float d = sqrt(p2.m_position.squaredDistanceTo(p1.m_position));
			float force = -m_fStiffness * (d - spring.m_initiallength);
			p1.m_force += (force * (p1.m_position - p2.m_position) / d);
			p2.m_force += (force * (p2.m_position - p1.m_position) / d);
		}
		for (auto& masspoint : m_masspoints)
		{
			masspoint.m_position += timeStep * masspoint.m_velocity * !masspoint.m_isfixed;
			applyBoundaryCollision(masspoint.m_position);
			masspoint.m_acceleration = masspoint.m_force / m_fMass + gravity;
			masspoint.m_velocity += masspoint.m_acceleration * timeStep;
			masspoint.m_force = Vec3(0, 0, 0);
		}
		break;
	case MIDPOINT:
		for (auto& spring : m_springs)
		{
			MassPoint& p1 = m_masspoints[spring.m_point1index];
			MassPoint& p2 = m_masspoints[spring.m_point2index];

			float d = sqrt(p2.m_position.squaredDistanceTo(p1.m_position));
			float force = -m_fStiffness * (d - spring.m_initiallength);
			p1.m_force += (force * (p1.m_position - p2.m_position) / d);
			p2.m_force += (force * (p2.m_position - p1.m_position) / d);
		}
		for (auto& masspoint : m_masspoints)
		{
			masspoint.m_midposition = masspoint.m_position + timeStep / 2 * masspoint.m_velocity * !masspoint.m_isfixed;
			applyBoundaryCollision(masspoint.m_midposition);
			masspoint.m_midvelocity = masspoint.m_velocity + masspoint.m_acceleration * timeStep / 2;
			masspoint.m_acceleration = masspoint.m_force / m_fMass + gravity;
			masspoint.m_force = Vec3(0, 0, 0);
		}

		for (auto& spring : m_springs)
		{
			MassPoint& p1 = m_masspoints[spring.m_point1index];
			MassPoint& p2 = m_masspoints[spring.m_point2index];

			float d = sqrt(p2.m_midposition.squaredDistanceTo(p1.m_midposition));
			float force = -m_fStiffness * (d - spring.m_initiallength);
			p1.m_force += (force * (p1.m_midposition - p2.m_midposition) / d);
			p2.m_force += (force * (p2.m_midposition - p1.m_midposition) / d);
		}
		for (auto& masspoint : m_masspoints)
		{
			masspoint.m_position += timeStep * masspoint.m_midvelocity * !masspoint.m_isfixed;
			applyBoundaryCollision(masspoint.m_position);
			masspoint.m_acceleration = masspoint.m_force / m_fMass + gravity;
			masspoint.m_velocity += masspoint.m_acceleration * timeStep;
			masspoint.m_force = Vec3(0, 0, 0);
		}
		break;
	case LEAPFROG:
		for (auto& spring : m_springs)
		{
			MassPoint& p1 = m_masspoints[spring.m_point1index];
			MassPoint& p2 = m_masspoints[spring.m_point2index];

			float d = sqrt(p2.m_position.squaredDistanceTo(p1.m_position));
			float force = -m_fStiffness * (d - spring.m_initiallength);
			p1.m_force += (force * (p1.m_position - p2.m_position) / d);
			p2.m_force += (force * (p2.m_position - p1.m_position) / d);
		}
		for (auto& masspoint : m_masspoints)
		{	
			//S.R. I think the start condition needs to be revisited for more accuracy. We need starting vel at t-h/2
			masspoint.m_acceleration = masspoint.m_force / m_fMass + gravity;
			masspoint.m_velocity += masspoint.m_acceleration * timeStep;
			masspoint.m_position += timeStep * masspoint.m_velocity;
			masspoint.m_force = Vec3(0, 0, 0);
		}


		break;
	default:
		break;
	}
}

void MassSpringSystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}

void MassSpringSystemSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}

void MassSpringSystemSimulator::setMass(float mass)
{
	this->m_fMass = mass;
}

void MassSpringSystemSimulator::setStiffness(float stiffness)
{
	this->m_fStiffness = stiffness;
}

void MassSpringSystemSimulator::setDampingFactor(float damping)
{
	this->m_fDamping = damping;
}

int MassSpringSystemSimulator::addMassPoint(Vec3 position, Vec3 Velocity, bool isFixed)
{
	int index = this->m_masspoints.size();
	this->m_masspoints.push_back(MassPoint(position, Velocity, isFixed));
	return index;
}

void MassSpringSystemSimulator::addSpring(int masspoint1, int masspoint2, float initialLength)
{
	this->m_springs.push_back(Spring(masspoint1, masspoint2, initialLength));
}

int MassSpringSystemSimulator::getNumberOfMassPoints()
{
	return this->m_masspoints.size();
}

int MassSpringSystemSimulator::getNumberOfSprings()
{
	return this->m_springs.size();
}

Vec3 MassSpringSystemSimulator::getPositionOfMassPoint(int index)
{
	return this->m_masspoints[index].m_position;
}

Vec3 MassSpringSystemSimulator::getVelocityOfMassPoint(int index)
{
	return this->m_masspoints[index].m_velocity;
}

void MassSpringSystemSimulator::applyExternalForce(Vec3 force)
{
}

void MassSpringSystemSimulator::applyBoundaryCollision(Vec3& position)
{
	position.x = position.x < -this->m_iboundary ? -this->m_iboundary : position.x;
	position.x = position.x > this->m_iboundary ? this->m_iboundary : position.x;
	position.y = position.y < -this->m_iboundary ? -this->m_iboundary : position.y;
	position.y = position.y > this->m_iboundary ? this->m_iboundary : position.y;
	position.z = position.z < -this->m_iboundary ? -this->m_iboundary : position.z;
	position.z = position.z > this->m_iboundary ? this->m_iboundary : position.z;
	
}

void MassSpringSystemSimulator::simpleCase()
{
	m_masspoints.clear();
	m_springs.clear();
	setMass(10.0f);
	setDampingFactor(0.0f);
	setStiffness(40.0f);
	this->m_iboundary = 500;
	applyExternalForce(Vec3(0, 0, 0));
	int p0 = addMassPoint(Vec3(0.0, 0.0f, 0), Vec3(-1.0, 0.0f, 0), false);
	int p1 = addMassPoint(Vec3(0.0, 2.0f, 0), Vec3(1.0, 0.0f, 0), false);
	addSpring(p0, p1, 1.0);
	this->gravity = Vec3(0, 0, 0);
}

void MassSpringSystemSimulator::createComplexCase(int minNumOfMassPoints, int maxNumOfMassPoints)
{
	this->gravity = Vec3(0, -10, 0);
	m_masspoints.clear();
	m_springs.clear();

	std::random_device rd;
	std::mt19937 eng(rd());
	std::uniform_int<int> randNumOfMassPoints(minNumOfMassPoints, maxNumOfMassPoints);

	int NumOfMassPoints = randNumOfMassPoints(eng);

	int minNumOfSprings = NumOfMassPoints;
	int maxNumOfSprings = NumOfMassPoints * (NumOfMassPoints + 1) / 2;

	
	std::uniform_real_distribution<float> randPosition(-5.0f, 5.0f);
	std::uniform_real_distribution<float> randVelocity(-15.0f, 15.0f);
	std::uniform_real_distribution<float> randIsFixed(0.0f,1.0f);
	std::uniform_int<int> randIndex(0, NumOfMassPoints - 1);
	std::uniform_int<int> randSpringNum(minNumOfSprings, maxNumOfSprings);
	std::uniform_real_distribution<float> randSpringStiffness(1.0f, 50.0f);
	std::uniform_real_distribution<float> randSpringInitialLength(1.0f, 20.0f);

	int numOfSprings = randSpringNum(eng);

	cout << "Num of mass points:\t" << NumOfMassPoints << endl;
	cout << "Num of springs: \t" << numOfSprings << endl;


	for (int i = 0; i < NumOfMassPoints; i++)
		m_masspoints.push_back(MassPoint(Vec3(randPosition(eng), randPosition(eng), randPosition(eng)),
			Vec3(randVelocity(eng), randVelocity(eng), randVelocity(eng)),
			(randIsFixed(eng)>0.5f)*fixpoints));
	
	std::map<std::pair<int,int>, bool> springCheck;

	for (int i = 0; i < NumOfMassPoints-1; i++)
	{
		m_springs.push_back(Spring(i, i + 1, randSpringInitialLength(eng)));
		springCheck[std::make_pair(i, i + 1)] = true;
		springCheck[std::make_pair(i + 1, i)] = true;
	}

	int count = 0;
	while(m_springs.size() < numOfSprings && count < 10000)
	{
		count++;
		int index1 = randIndex(eng);
		int index2 = randIndex(eng);
		
		if (index1 == index2) continue;
		if (springCheck[std::make_pair(index1, index2)] || springCheck[std::make_pair(index2, index1)]) continue;
		
		springCheck[std::make_pair(index1, index2)] = true;
		springCheck[std::make_pair(index2, index1)] = true;
		m_springs.push_back(Spring(index1, index2, randSpringInitialLength(eng)));
	}
	
}
