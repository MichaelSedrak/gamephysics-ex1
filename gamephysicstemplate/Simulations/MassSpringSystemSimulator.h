#ifndef MASSSPRINGSYSTEMSIMULATOR_h
#define MASSSPRINGSYSTEMSIMULATOR_h
#include "Simulator.h"

// Do Not Change
#define EULER 0
#define LEAPFROG 1
#define MIDPOINT 2
// Do Not Change

struct Spring{
	int m_point1index;
	int m_point2index;
	float m_initiallength;
	Spring(int p1index, int p2index, float initiallength) 
		: m_point1index(p1index),m_point2index(p2index),m_initiallength(initiallength)
	{}
};

struct MassPoint {
	Vec3 m_position;
	Vec3 m_midposition;
	Vec3 m_velocity;
	Vec3 m_midvelocity;
	Vec3 m_acceleration;
	Vec3 m_force;
	bool m_isfixed;
	MassPoint(Vec3 position, Vec3 velocity, bool isfixed)
		: m_position(position), m_velocity(velocity), m_isfixed(isfixed)
	{}
};

class MassSpringSystemSimulator:public Simulator{
public:
	// Construtors
	MassSpringSystemSimulator();
	
	// UI Functions
	const char * getTestCasesStr();
	const char* getIntegratorsStr();
	void initUI(DrawingUtilitiesClass * DUC);
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void notifyCaseChanged(int testCase);
	void externalForcesCalculations(float timeElapsed);
	void simulateTimestep(float timeStep);
	void onClick(int x, int y);
	void onMouse(int x, int y);
	

	// Specific Functions
	void setMass(float mass);
	void setStiffness(float stiffness);
	void setDampingFactor(float damping);
	int addMassPoint(Vec3 position, Vec3 Velocity, bool isFixed);
	void addSpring(int masspoint1, int masspoint2, float initialLength);
	int getNumberOfMassPoints();
	int getNumberOfSprings();
	Vec3 getPositionOfMassPoint(int index);
	Vec3 getVelocityOfMassPoint(int index);
	void applyExternalForce(Vec3 force);
	void applyBoundaryCollision(Vec3& position);

	void simpleCase();
	void createComplexCase(int minNumOfMassPoints, int maxNumOfMassPoints);
	
	// Do Not Change
	void setIntegrator(int integrator) {
		m_iIntegrator = integrator;
	}

private:
	// Data Attributes
	float m_fMass;
	float m_fStiffness;
	float m_fDamping;
	int m_iIntegrator;
	int m_iboundary;
	bool fixpoints;
	Vec3 gravity = Vec3(0, -10, 0);
	std::vector<MassPoint> m_masspoints;
	std::vector<Spring> m_springs;

	// UI Attributes
	Vec3 m_externalForce;
	Point2D m_mouse;
	Point2D m_trackmouse;
	Point2D m_oldtrackmouse;
	Vec3  m_vfMovableObjectPos;
	Vec3  m_vfMovableObjectFinalPos;
	float m_fSphereSize;
};
#endif